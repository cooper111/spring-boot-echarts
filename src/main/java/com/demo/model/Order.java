package com.demo.model;

import lombok.Data;

/**
 * @author Kevin
 * @date 2020/4/21 19:46
 *
 */
@Data
public class Order{
    public Integer day;//1-7
    public String name;
    public Integer num;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
