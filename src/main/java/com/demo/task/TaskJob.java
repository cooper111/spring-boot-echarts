package com.demo.task;

import cn.hutool.core.date.DateUtil;
import com.demo.mapper.CronMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author Kevin
 * @date 2020/4/22 0:26
 */
@Component
@Slf4j
public class TaskJob {
    @Resource
    private CronMapper cronMapper;

    // 表示每个星期天晚上11点59分59秒触发更新
    @Scheduled(cron = "59 59 23 ? * SUN")
    public void job1() {
        cronMapper.transferTo();
        cronMapper.deleteOrder();
    }


//    /**
//     * 按照标准时间来算，每隔 10s 执行一次
//     */
//    @Scheduled(cron = "0/10 * * * * ?")
//    public void job1() {
//        log.info("【job1】开始执行：{}", DateUtil.formatDateTime(new Date()));
//    }
//
//    /**
//     * 从启动时间开始，间隔 2s 执行
//     * 固定间隔时间
//     */
//    @Scheduled(fixedRate = 2000)
//    public void job2() {
//        log.info("【job2】开始执行：{}", DateUtil.formatDateTime(new Date()));
//    }

    /**
     * 从启动时间开始，延迟 5s 后间隔 4s 执行
     * 固定等待时间
     */
    @Scheduled(fixedDelay = 4000, initialDelay = 5000)
    public void job3() {
        log.info("【job3】开始执行：{}", DateUtil.formatDateTime(new Date()));
    }
}
