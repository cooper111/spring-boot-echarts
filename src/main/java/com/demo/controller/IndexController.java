package com.demo.controller;

import com.demo.model.Index;
import com.demo.Vo.IndexExtra;
import com.demo.service.Impl.OrderServiceImpl;
import com.demo.service.IndexService;
import com.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhouyulong
 * @date 2018/10/28 下午2:43.
 */
@Controller
public class IndexController {
    @Resource
    private OrderService orderService;
    @Autowired
    private IndexService indexService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }
    @RequestMapping(value = "/pie", method = RequestMethod.GET)
    public String pie() {
        return "pie";
    }
    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String brokenLine() {
        return "brokenLine";
    }

    //用于Echarts折线图返回值
    @ResponseBody
    @RequestMapping(value = "/weekly", method = RequestMethod.GET)
    public List<OrderServiceImpl.OrderVo> weeklyCount() {
        List<OrderServiceImpl.OrderVo> orders = orderService.weeklyCount();
        return orders;
    }
    //用于Echarts表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getList() {
        Map<String,Object> map = new HashMap<>();
        map.put("msg", "ok");
        map.put("data", indexService.findAll());
        return map;
    }
    //用于Echarts表
    @RequestMapping(value = "/list1", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getList1() {
        List<Index> products = indexService.findAll();
        List<IndexExtra> temp = new ArrayList<>();
        for (Index index: products) {
            temp.add(new IndexExtra(index));
        }
        Map<String,Object> map = new HashMap<>();
        map.put("msg", "ok");
        map.put("data", temp);
        return map;
    }
    //用于表格展示
    @GetMapping("/listAll")
    public String getAllProducts(Model model) {
        List<Index> products = indexService.findAll();
        model.addAttribute("products", products);
        return "list";
    }
}
