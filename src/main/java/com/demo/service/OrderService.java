package com.demo.service;

import com.demo.model.Order;
import com.demo.service.Impl.OrderServiceImpl;

import java.util.List;

/**
 * @author Kevin
 * @date 2020/4/21 20:36
 */
public interface OrderService {
    List<OrderServiceImpl.OrderVo> weeklyCount();
}
