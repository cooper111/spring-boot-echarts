package com.demo.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * @author Kevin
 * @date 2020/4/21 23:18
 */
@Mapper
public interface CronMapper {

    // 定时任务1：将订单表数据处理至统计表（每周一次）
    @Update("update my_chart01  inner join(SELECT name,Sum(num) as num from my_chart02 group by name) A on my_chart01.name = A.name set my_chart01.num = my_chart01.num + A.num")
    void transferTo();

    // 定时任务2：将订单表清空（每周一次）
    @Delete("delete from my_chart02")
    void deleteOrder();
}
