package com.demo.controller;

import com.demo.mapper.CronMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author Kevin
 * @date 2020/4/21 23:41
 */
@Controller
@RequestMapping(value = "/test")
public class TestController {
    @Resource
    CronMapper cronMapper;

    @ResponseBody
    @RequestMapping("/update")
    public String transferTo() {
        cronMapper.transferTo();
        return "订单表汇总完成";
    }
}
