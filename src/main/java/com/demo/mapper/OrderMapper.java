package com.demo.mapper;

import com.demo.model.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Kevin
 * @date 2020/4/21 19:56
 */
@Mapper
public interface OrderMapper {
    //查询单个订单的先不写了，这里的目的是统计各个商品在一周的各天内的销量

    @Select("SELECT name,day,Sum(num) as num from my_chart02 group by name, day")
    List<Order> weeklyCount();
}
