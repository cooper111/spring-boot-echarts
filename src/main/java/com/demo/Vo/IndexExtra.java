package com.demo.Vo;

import com.demo.model.Index;

/**
 * @author Kevin
 * @date 2020/4/21 2:14
 * 解决Index字段和js字段重复问题
 */
public class IndexExtra {
    public String name;
    public Integer value;

    public IndexExtra(Index index) {
        this.name = index.getName();
        this.value = index.getNum();
    }
}
